//固件版本号
#define Firmware_version "V 1.3.0"

//是否开启debug功能
const unsigned long BAUD_RATE = 115200;   //波特率

#define DEBUG 

#ifdef DEBUG 
#define DebugPrintln(message) Serial.println(message)
#else 
#define DebugPrintln(message)
#endif

#ifdef DEBUG 
#define DebugPrint(message) Serial.print(message) 
#else 
#define DebugPrint(message)
#endif

//星座信息

//默认WIFI信息
char ssid[] = "liuzewen";
char pswd[] = "17609245102liu";

//GPIO配置
#define LED         D4 //gpio2
#define LED_PIN     8  //WS2812点阵屏引脚
#define ADC_PIN     A0 //电量检测ADC引脚

//设置WS2812 RGB灯排列方式，数目，长，宽
#define COLOR_ORDER GRB
#define CHIPSET     WS2811
#define BRIGHTNESS 60    //设置RGB亮度
const uint8_t kMatrixWidth =  8;//高Y
const uint8_t kMatrixHeight = 32;//宽X
const bool    kMatrixSerpentineLayout = true;//true为S形点阵(|/|/|/|) false为一字形(|||||||)
#define NUM_LEDS (kMatrixWidth * kMatrixHeight)

#define LED_OFF   digitalWrite(LED, HIGH)//关灯
#define LED_ON    digitalWrite(LED, LOW)//开灯
#define LED_PWM   digitalWrite(LED, !digitalRead(LED))//灯闪烁
#define BEEP_ON   digitalWrite(BEEP, HIGH)//蜂鸣器响
#define BEEP_OFF  digitalWrite(BEEP, LOW)//蜂鸣器停
#define BEEP_PWM  digitalWrite(BEEP, !digitalRead(BEEP))//警报

#define UP_KEY      D6 //gpio12
#define M_KEY       D5 //gpio14
#define DOWN_KEY    D7 //gpio13
#define WAKE_UP     D3 //gpio0

//读取键值
#define KEYU  digitalRead(UP_KEY)
#define KEYM  digitalRead(M_KEY)
#define KEYD  digitalRead(DOWN_KEY)
#define KEYW  digitalRead(WAKE_UP)

#define NO_KEY_PRES     0
#define UP_KEY_PRES     1
#define M_KEY_PRES      2
#define DOWN_KEY_PRES   3
#define WAKE_UP_PRES    4

#define UP_KEY_LONG_PRES     11
#define M_KEY_LONG_PRES      22
#define DOWN_KEY_LONG_PRES   33
#define WAKE_UP_LONG_PRES    44

long up_pres_time = 0;
long m_pres_time = 0;
long down_pres_time = 0;
long wake_pres_time = 0;

//定义时间结构体变量TIME
struct TIME_CONFIG{
  uint8_t weeks = 1;
  uint32_t years = 2020;
  uint8_t months = 3;
  uint8_t days = 4;
  uint8_t hours = 15;
  uint8_t minutes = 5;
  uint8_t seconds = 30;
}TIME_NOW;

//定义bilibili结构体，用来存储粉丝，播放量，点赞
struct BILIBILI_DATE{
  uint32_t follower = 0;
  uint32_t following = 0;
  uint32_t view = 0;
  uint32_t likes = 0;
}BILI_NOW_DATE;

//时间模式（本地DS3231源 或  NTP网络源）
#define DS3231_TIME_MODE  0
#define NTP_TIME_MODE     1
bool TIME_MODE = NTP_TIME_MODE;//默认为NTP时间

//是否更新各种信息
bool DATA_UPDATA = 0;

//决定显示哪个桌面
uint8_t Desktop_Mode = 1;
#define TIME_WEATHER   1
#define BILIBILI       2

bool RGB_KEY = 1;//RGB点阵开关

//城市和cityid对应关系
struct CITY_DEFINE{   
  char city_name[16];//城市name
  char city_id[12]; //天气ID
};
#define city_num 4//城市个数
uint8_t which_city = 1;//当前是哪个城市的天气
struct CITY_DEFINE citys[city_num]; //城市列表

//天气数据
struct UserData{   
  char city[16];//城市
  char weather_code[4];//天气
  char temp[5];//温度
  uint8_t weather;//实际天气
}; 
UserData userData;

//wifi结构体
struct config_type{   
  char stassid[32];   
  char stapsw[64]; 
}config;

//定义页码&光标
struct page_define{   
  char name[32];                  //当前页码名称
  uint8_t cursor = 1;             //此时光标所在行数
  uint8_t rows = 1;               //共有多少行
  long refresh_time = 2*100;      //设置模式OLED刷新时间间隔，帧率 = 1000/refresh_time = 5
  long time = 0;                  //页面时间
}set_up_page;

uint8_t Cursor_position = 1;                           //此时光标所在行数
uint8_t Number_of_rows = 1;                            //共有多少行
uint16_t OLED_Brightness = 20;                        //设置OLED显示屏亮度

uint16_t Battery_capacity = 500;                         //电池电量

#ifndef __GETWEATHER_H
#define __GETWEATHER_H

//定义气象种类
 #define WEATHER_CODE_DAY_SUN "0" // 晴 （ 国 内 城 市 白 天 晴 ）
 #define WEATHER_CODE_NIGHT_SUN "1" // 晴 （ 国 内 城 市 夜 晚 晴 ）
 #define WEATHER_CODE_DAY_SUN1 "2" // 晴 （ 国 外 城 市 白 天 晴 ）
 #define WEATHER_CODE_NIGHT_SUN2 "3" // 晴 （ 国 外 城 市 夜 晚 晴 ）
 #define WEATHER_CODE_CLOUDY "4" // 多 云
 #define WEATHER_CODE_DAY_PARTLY_CLOUDY "5" // 白 天 晴 间 多 云
 #define WEATHER_CODE_NIGHT_PARTLY_CLOUDY "6" // 夜 晚 晴 间 多 云
 #define WEATHER_CODE_DAY_MOSTLY_CLOUDY "7" // 白 天 大 部 多 云
 #define WEATHER_CODE_NIGHT_MOSTLY_CLOUDY "8" // 夜 晚 大 部 多 云
 #define WEATHER_CODE_OVERCAST "9" // 阴
 #define WEATHER_CODE_SHOWER "10" // 阵 雨
 #define WEATHER_CODE_THUNDERSHOWER "11" // 雷 阵 雨
 #define WEATHER_CODE_THUNDERSHOWER_WITH_HAIL "12" // 雷 阵 雨 伴 有 冰 雹
 #define WEATHER_CODE_LIGHT_RAIN "13" // 小 雨
 #define WEATHER_CODE_MODERATE_RAIN "14" // 中 雨
 #define WEATHER_CODE_HEAVY_RAIN "15" // 大 雨
 #define WEATHER_CODE_STORM "16" // 暴 雨
 #define WEATHER_CODE_HEAVY_STORM "17" // 大 暴 雨
 #define WEATHER_CODE_SEVERE_STORM "18" // 特 大 暴 雨
 #define WEATHER_CODE_ICE_RAIN "19" // 冻 雨
 #define WEATHER_CODE_SLEET "20" // 雨 夹 雪
 #define WEATHER_CODE_SNOW_FLURRY "21" // 阵 雪
 #define WEATHER_CODE_LIGHT_SNOW "22" // 小 雪
 #define WEATHER_CODE_MODERATE_SNOW "23" // 中 雪
 #define WEATHER_CODE_HEAVY_SNOW "24" // 大 雪
 #define WEATHER_CODE_SNOW_STORM "25" // 暴 雪
 
//定义天气的类型
#define SUN_DAY 0 
#define SUN_NIGHT 1 
#define SUN_CLOUD 2 
#define CLOUD 3 
#define RAIN 4 
#define THUNDER 5

#endif

//各种标志位
bool Boot_up = 1;//开机标志位
bool flag = HIGH;//默认当前灭灯
bool Get_NTPclient = 0;//是否获取NTPclient标志位
bool Get_SmartConfig = 0;//是否配网标志位
bool WIFI_MODE = 0;//是否有网络
bool OTA_MODE = 0;//判断是否无线升级程序
/**
 * 设置显示器的旋转角度
 * @param U8G2_OLED_R @param u8g2_cb 旋转选项
 *        0   U8G2_R0 不做旋转
 *        1   U8G2_R1 旋转90度
 *        2   U8G2_R2 旋转180度
 *        3   U8G2_R3 旋转270度
 *        4   U8G2_MIRROR 不做旋转 水平，显示内容是镜像的，暂时不理解
 */
bool U8G2_OLED_R = 0;//设置显示器方向

//刷新时间(ms)
long LastWeatherTime = 0;       //获取天气
long LastRtcTime = 0;           //从DS3231读时间
long LastNTPTime = 0;           //从网络获取时间
long set_mode_time = 0;         //设置模式OLED刷新时间间隔
long ntpget = 0;                //网络时间

// 请 求 服 务 间 隔
long WeatherDelay = 5*60*1000;        //天气获取周期(ms)
long RtcDelay = 1*1000;               //时钟获取周期(ms)
long openOledTime = 5*1000;           //每次亮屏时间(ms)
long key_long_pres_time = 1.2*1000;   //按键长时间按下时间
long set_mode_refresh_time = 1*100;   //设置界面刷新时间(ms)
long NTPGET = 1*1000;                 //网络时间更新时间

//心知天气
const unsigned long HTTP_TIMEOUT = 5000;                       // max respone time from server 
const size_t MAX_CONTENT_SIZE = 500;                           // max size of the HTTP response 
const char* host = "api.seniverse.com";                        //心知天气官网
const char* APIKEY = "umsnxrv2snsd68t3";                       //用户API KEY
const char* city = "xian";                                     //城市
const char* language = "zh-Hans";                              //zh-Hans
char response[MAX_CONTENT_SIZE]; 
char endOfHeaders[] = "\r\n\r\n";

//哔哩哔哩API
const char* bilibili_HOST = "http://api.bilibili.com";
const char* bilibili_UID = "277392717";